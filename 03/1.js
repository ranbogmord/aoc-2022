import {readInput} from "../helpers.js";

const input = readInput(true).split("\n")

const dupes = input.reduce((acc, item) => {
  const parts = [
    item.slice(0, item.length / 2),
    item.slice(item.length / 2, item.length)
  ];

  const dupes = parts[0].split('').reduce((acc2, char) => {
    if (parts[1].includes(char) && !acc2.includes(char)) {
      acc2.push(char);
    }

    return acc2;
  }, []);

  acc = acc.concat(dupes);
  return acc;
}, []);

const total = dupes.reduce((acc, item) => {
  acc += item.match(/[A-Z]/) ? item.charCodeAt(0) - 38 : item.charCodeAt(0) - 96;
  return acc;
}, 0);
console.log(total);
