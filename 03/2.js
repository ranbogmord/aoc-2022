import {chunk, readInput} from "../helpers.js";

const input = readInput(true).split("\n");

const groups = chunk(input, 3);
const scores = groups.reduce((acc, group) => {
  const g = group.map(x => x.split(""));
  const local = [];
  g[0].forEach((char) => {
    if (g[1].includes(char) && g[2].includes(char) && !local.includes(char)) {
      local.push(
        char
      );
    }
  })

  return acc.concat(local.map(char => char.match(/[A-Z]/) ? char.charCodeAt(0) - 38 : char.charCodeAt(0) - 96));
}, []);

console.log(scores.reduce((acc, score) => acc + score, 0))
