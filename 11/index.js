import { readInputFile, runPart } from '../helpers.js';

const input = readInputFile().split("\n");

const monkeys = [];
let monkey = {};
for (let i = 0; i < input.length; i++) {
  const row = input[i];
  if (row.match(/Monkey \d:/)) {
    monkey = {
      items: input[i+1].replace(/Starting items: /, '').replace(' ', '').split(',').map(x => parseInt(x)),
      operation: input[i+2].replace(/Operation: new = /, '').trim(),
      mod: parseInt(input[i+3].match(/(\d+)/)[1]),
      onTrue: parseInt(input[i+4].match(/(\d+)/)[1]),
      onFalse: parseInt(input[i+5].match(/(\d+)/)[1]),
      inspections: 0
    };
    i += 5
    monkeys.push(monkey);
  }
}

const tossItems = (rows, rounds = 20, superModulo = undefined) => {
  for (let i = 0; i < rounds; i++) {
    for (let m of rows) {
      let item = null;
      while(item = m.items.shift()) {
        m.inspections += 1;
        let old = item;
        let newVal = parseInt(eval(m.operation)); // yeah... lol
        if (!superModulo) {
          newVal = Math.floor(newVal / 3);
        } else {
          newVal = newVal % superModulo;
        }

        if (newVal % m.mod === 0) {
          rows[m.onTrue].items.push(newVal);
        } else {
          rows[m.onFalse].items.push(newVal);
        }
      }
    }
  }

  return rows;
};

runPart(1, () => {
  let tossResult = tossItems(monkeys);
  const result = tossResult.sort((a, b) => a.inspections < b.inspections ? 1 : -1).map(x => x.inspections).slice(0, 2).reduce((acc, item) => {
    return acc * item;
  }, 1);
  console.log("Part 1:", result)
});
runPart(2, () => {
  let superModulo = monkeys.reduce((acc, item) => {
    return acc * item.mod;
  }, 1);
  
  let tossResult = tossItems(monkeys, 10000, superModulo);
  const result = tossResult.sort((a, b) => a.inspections < b.inspections ? 1 : -1).map(x => x.inspections).slice(0, 2).reduce((acc, item) => {
    return acc * item;
  }, 1);
  console.log("Part 2:", result);
});

