import {readInputFile, runPart} from "../helpers.js";

const input = readInputFile().split("\n").map(x => x.split("").map(y => parseInt(y, 10)));

runPart(1, () => {
  const isVisible = (matrix, x, y) => {
    // Check edges
    if (x === 0 || y === 0 || y === matrix.length - 1 || x === matrix[0].length - 1) {
      return true;
    }

    const cell = matrix[y][x];

    let visibleLeft = true, visibleRight = true, visibleTop = true, visibleBottom = true;

    // check visible left
    for (let i = 0; i < x; i++) {
      if (cell <= matrix[y][i]) {
        visibleLeft = false;
      }
    }

    // check visible right
    for (let i = x + 1; i < matrix[0].length; i++) {
      if (cell <= matrix[y][i]) {
        visibleRight = false;
      }
    }

    for (let i = 0; i < y; i++) {
      if (cell <= matrix[i][x]) {
        visibleTop = false;
      }
    }

    for (let i = y + 1; i < matrix.length; i++) {
      if (cell <= matrix[i][x]) {
        visibleBottom = false;
      }
    }

    return visibleLeft || visibleRight || visibleTop || visibleBottom;
  }

  let visible = 0;
  for (let y = 0; y < input.length; y++) {
    for (let x = 0; x < input[0].length; x++) {
      if (isVisible(input, x, y)) {
        visible += 1;
      }
    }
  }
  console.log("Part 1:", visible);
});

runPart(2, () => {

  const getScore = (matrix, x, y) => {
    const cell = matrix[y][x];

    let scoreLeft = 0, scoreRight = 0, scoreTop = 0, scoreBottom = 0;

    // left
    if (x > 0) {
      let prevIdx = x - 1;

      while (prevIdx >= 0) {
        scoreLeft += 1;
        if (matrix[y][prevIdx] >= cell) {
          break;
        }
        prevIdx -= 1;
      }
    }

    // right
    if (x < matrix[0].length - 2) {
      let nextIdx = x + 1;

      while (nextIdx < matrix[0].length) {
        scoreRight += 1;
        if (matrix[y][nextIdx] >= cell) {
          break;
        }
        nextIdx += 1;
      }
    }

    // top
    if (y > 0) {
      let prevIdx = y - 1;

      while (prevIdx >= 0) {
        scoreTop += 1;
        if (matrix[prevIdx][x] >= cell) {
          break;
        }
        prevIdx -= 1;
      }
    }

    // bottom
    if (y < matrix.length - 1) {
      let nextIdx = y + 1;
      while (nextIdx < matrix.length) {
        scoreBottom += 1;
        if (matrix[nextIdx][x] >= cell) {
          break;
        }
        nextIdx += 1;
      }
    }

    return scoreLeft * scoreRight * scoreTop * scoreBottom;
  }

  const maxFound = input.reduce((total, row, y) => {
    let rowTotal = row.reduce((rowMax, cell, x) => {
      let score = getScore(input, x, y);
      if (score > rowMax) {
        return score;
      } else {
        return rowMax;
      }
    });

    if (rowTotal > total) {
      return rowTotal;
    } else {
      return total;
    }
  }, 0);

  console.log("Part 2:", maxFound);
});
