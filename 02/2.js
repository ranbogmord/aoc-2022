import {readInput} from "../helpers.js";

const input = readInput(true);

const scores = {
  "ax": 3,
  "ay": 4,
  "az": 8,
  "bx": 1,
  "by": 5,
  "bz": 9,
  "cx": 2,
  "cy": 6,
  "cz": 7
};

const sum = input.split("\n").reduce((acc, row) => {
  const key = row.toLowerCase().replace(' ', '');

  return acc + scores[key];
}, 0);

console.log(sum)
