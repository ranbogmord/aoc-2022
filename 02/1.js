import {readInput} from "../helpers.js";

const input = readInput(true);

const scores = {
  "ax": 1 + 3,
  "ay": 2 + 6,
  "az": 3,
  "bx": 1,
  "by": 2 + 3,
  "bz": 3 + 6,
  "cx": 1 + 6,
  "cy": 2,
  "cz": 3 + 3
};

const sum = input.split("\n").reduce((acc, row) => {
  const key = row.toLowerCase().replace(' ', '');

  return acc + scores[key];
}, 0);

console.log(sum)
