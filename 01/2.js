import {readInput} from "../helpers.js";
const input = readInput(true);

const elves = [];
let current = 0;
input.split("\n").forEach(row => {
  if (!row) {
    elves.push(current);
    current = 0;
  } else {
    current += parseInt(row, 10);
  }
});

const sorted = elves.sort((a, b) => a < b ? 1 : -1);

console.log(sorted[0] + sorted[1] + sorted[2]);
