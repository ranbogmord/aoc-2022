import {readInput} from "../helpers.js";

const input = readInput(true);

let max = 0;
let current = 0;
input.split("\n").forEach(row => {
  if (!row) {
    if (current > max) {
      max = current;
    }
    current = 0;
  } else {
    current += parseInt(row, 10);
  }
});

console.log(max);
