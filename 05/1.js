import {readInput} from "../helpers.js";

const input = readInput(true);

const [state, steps] = input.split(/^\n/m);
const parsed = state.replace(/[\[\]]/g, ' ').split('\n').reduce((acc, row, rowIdx) => {
  row.split('').forEach((cell, colIdx) => {
    if (!acc[colIdx]) {
      acc[colIdx] = [];
    }
    if (cell.match(/[A-Z]/)) {
      acc[colIdx].push(cell);
    }
    if (cell.match(/[0-9]/)) {
      acc.result[cell] = [...acc[colIdx]];
    }
  })

  return acc;
}, {
  result: {}
});

steps.split('\n').forEach(step => {
  const [match, count, from, to] = step.match(/move (\d+) from (\d+) to (\d+)/);

  for (let i = 0; i < count; i++) {
    parsed.result[to] = [...parsed.result[from].splice(0, 1), ...parsed.result[to]];
  }

});
let str = "";
for (let key in parsed.result) {
  str = `${str}${parsed.result[key][0]}`
}
console.log(str)
