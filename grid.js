import {PriorityQueue} from "./pqueue.js";

export class Point {
  constructor(x, y, value) {
    this.x = x;
    this.y = y;
    this.value = value;
  }

  isOn(targetPoint) {
    return this.x === targetPoint.x && this.y === targetPoint.y;
  }

  toString() {
    return `${this.x},${this.y}`;
  }

  clone(value = null) {
    return new Point(this.x, this.y, value || this.value);
  }

  translate(dx, dy) {
    this.x += dx;
    this.y += dy;
  }

  distanceTo(point) {
    const dX = Math.abs(this.x - point.x);
    const dY = Math.abs(this.y - point.y);

    return dX + dY;
  }

  static fromCoords(coords, value = undefined) {
    const [x, y] = coords.split(',').map(x => parseInt(x));
    return new Point(x, y, value);
  }
}

export class Grid {
  constructor(points, params = {}) {
    this.points = points;

    const {minX = 0, minY = 0} = params;

    this.minX = minX;
    this.minY = minY;
  }

  getNeighbours(point, diagonal = false) {
    const directions = [
      [-1, 0], [0, -1], [1, 0], [0, 1],
      ...(diagonal ? [
        [-1, -1], [1, -1], [1, 1], [-1, 1]
      ] : [])
    ];

    return directions.map(([dx, dy]) => [point.x + dx, point.y + dy])
      .filter(([x, y]) => this.hasPoint(x, y))
      .map(([x, y]) => this.get(x, y));
  }

  hasPoint(x, y) {
    return !!this.points[y] && !!this.points[y][x];
  }

  get(x, y) {
    return this.points[y][x];
  }

  set(point) {
    if (!this.points[point.y]) {
      this.points[point.y] = [];
    }

    this.points[point.y][point.x] = point;
  }

  getByCoords(coords) {
    const [x, y] = coords.split(',').map(x => +x);
    if (!this.hasPoint(x, y)) {
      return undefined;
    }

    return this.get(x, y);
  }

  forEachPoint(callback) {
    this.points.forEach(row => row.forEach(callback));
  }

  dimensions() {
    let points = [];
    this.forEachPoint(p => points.push(p));

    return points.reduce((acc, point) => {
      if (!acc) {
        return {
          topLeft: {
            x: point.x,
            y: point.y
          },
          bottomRight: {
            x: point.x,
            y: point.y
          }
        }
      }

      if (point.x < acc.topLeft.x) {
        acc.topLeft.x = point.x;
      }
      if (point.x > acc.bottomRight.x) {
        acc.bottomRight.x = point.x;
      }
      if (point.y < acc.topLeft.y) {
        acc.topLeft.y = point.y;
      }
      if (point.y > acc.bottomRight.y) {
        acc.bottomRight.y = point.y;
      }

      return acc;
    }, null);
  }

  djikstra(startingPoint, {isTargetPoint, getWeight = () => 1, isWalkable = () => true}) {
    const open = new PriorityQueue();
    const pathMap = new Map();
    const visited = new Set();

    this.forEachPoint((point) => {
      const pointStr = point.toString();
      const distance = point.isOn(startingPoint) ? 0 : Infinity;

      open.add({value: pointStr, priority: distance});
      pathMap.set(pointStr, {
        pointStr,
        distance
      });
    });

    do {
      const item = open.next();
      const point = this.getByCoords(item.value);

      if (visited.has(item.value)) {
        continue;
      }

      if (isTargetPoint && isTargetPoint(point)) {
        break; // possibly continue?
      }

      visited.add(item.value);

      const neighbours = this.getNeighbours(point).filter(n => isWalkable(n, point));
      for (const neighbour of neighbours) {
        const nPathInfo = pathMap.get(neighbour.toString());

        if (visited.has(neighbour.toString()) || !nPathInfo) {
          continue;
        }

        const newPriority = item.priority + getWeight(neighbour);
        if (newPriority < nPathInfo.distance) {
          open.add({
            value: nPathInfo.pointStr,
            priority: newPriority
          });
          nPathInfo.distance = newPriority;
          nPathInfo.parent = point.toString();
          pathMap.set(nPathInfo.pointStr, nPathInfo);
        }
      }
    } while (open.length() > 0);

    return pathMap;
  }

  render() {
    const out = [];
    const dim = this.dimensions();

    for (let y = dim.topLeft.y; y < this.points.length; y++) {
      const row = [];
      for (let x = dim.topLeft.x; x < this.points[y].length; x++) {
        row.push(this.points[y][x]?.value || ' ');
      }
      out.push(row);
    }

    console.log(out.map(row => row.join(' ')).join("\n"))
  }
}
