import {readInput} from "../helpers.js";

const input = readInput(true);

const pairs = input.split("\n");
const total = pairs.reduce((acc, pair) => {
  let parts = pair.split(",");
  parts = parts.map(p => {
    const [from, to] = p.split("-");
    const nums = [];
    for (let i = parseInt(from, 10); i <= parseInt(to, 10); i++) {
      nums.push(i);
    }
    return nums;
  }).sort((a, b) => a.length > b.length ? -1 : 1);

  if (parts[1].every(num => parts[0].includes(num))) {
    acc += 1;
  }

  return acc;
}, 0);
console.log(total);

