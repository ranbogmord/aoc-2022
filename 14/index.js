import { readInputFile, runPart } from '../helpers.js';
import {Grid, Point} from "../grid.js";

const input = readInputFile().split("\n").map(x => x.trim().split(" -> ").map(coords => {
  const [x, y] = coords.split(',').map(item => parseInt(item));
  return new Point(x, y, "#");
}));

const initializeGrid = (knownPaths) => {
  const allPoints = knownPaths.reduce((acc, path) => {
    return acc.concat(path);
  }, []);

  const dimensions = allPoints.reduce((acc, point) => {
    if (!acc) {
      return {
        minX: point.x,
        maxX: point.x,
        minY: point.y,
        maxY: point.y,
      }
    }

    if (point.x < acc.minX) {
      acc.minX = point.x;
    }
    if (point.x > acc.maxX) {
      acc.maxX = point.x;
    }
    if (point.y < acc.minY) {
      acc.minY = point.y;
    }
    if (point.y > acc.maxY) {
      acc.maxY = point.y;
    }

    return acc;
  }, null)

  const gridPoints = [];
  for (let y = 0; y <= dimensions.maxY; y++) {
    gridPoints[y] = [];
    for (let x = dimensions.minX; x <= dimensions.maxX; x++) {
      gridPoints[y][x] = new Point(x, y, '.');
    }
  }

  const grid = new Grid(gridPoints, {
    minX: dimensions.minX,
  });

  for (let path of knownPaths) {
    for (let i = 1; i < path.length; i++) {
      const last = path[i - 1];
      const current = path[i];

      let prop = "x";
      if (last.x === current.x) {
        prop = "y";
      }

      let start = Math.min(last[prop], current[prop]);
      const end = Math.max(last[prop], current[prop]);

      do {
        start += 1;
        const p = last.clone();
        p[prop] = start;
        allPoints.push(p);
      } while (start < end - 1);
    }
  }

  allPoints.forEach(point => grid.set(point));

  return grid;
};

runPart(1, () => {
  const grid = initializeGrid(input);
  const emitter = new Point(500, 0, '+');

  const directions = [
    [0, 1], [-1, 1], [1, 1]
  ];

  let steps = 0;
  let outside = false;

  while (!outside) {
    let moved = true;
    steps += 1;

    const point = emitter.clone('o');
    while (moved) {
      moved = false;
      for (const [dx, dy] of directions) {
        const newX = point.x + dx, newY = point.y + dy;

        if (!grid.hasPoint(newX, newY)) {
          outside = true;
          moved = false;
          break;
        }

        const current = grid.get(newX, newY);
        if (current.value === ".") {
          point.translate(dx, dy);
          moved = true;
          break;
        }
      }
    }
    grid.set(point);
  }

  console.log("Part 1:", steps - 1);
});
runPart(2, () => {
  const grid = initializeGrid(input);
  const dim = grid.dimensions();

  const emitter = new Point(500, 0, '+');

  const directions = [
    [0, 1], [-1, 1], [1, 1]
  ];

  let steps = 0;
  let onEmitter = false;

  while (!onEmitter) {
    let moved = true;
    steps += 1;

    const point = emitter.clone('o');
    while (moved) {
      moved = false;
      for (const [dx, dy] of directions) {
        const newX = point.x + dx, newY = point.y + dy;

        if (!grid.hasPoint(newX, newY)) {
          let value = ".";
          if (newY === dim.bottomRight.y + 2) {
            value = "#";
          }
          grid.set(new Point(newX, newY, value));
        }

        const current = grid.get(newX, newY);

        if (current.value === ".") {
          point.translate(dx, dy);
          moved = true;
          break;
        }
      }

      if (!moved) {
        onEmitter = point.isOn(emitter);
      }
    }
    grid.set(point);
  }

  console.log("Part 2:", steps);
});

