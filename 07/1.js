import {deepClone, readInput} from "../helpers.js";
import _ from "lodash/object.js";

const input = readInput(true).split("\n");

const struct = {};
const defaultDir = {
  dirs: {},
  files: [],
  totalSize: 0
};

let currentDir = deepClone(defaultDir);

let currentPath = "root";
for (let row of input) {
  if (row.match(/^\$/)) {
    let [cmd, arg] = row.replace(/^\$ /, '').split(" ");
    if (cmd === "cd") {
      _.set(struct, currentPath, currentDir);

      if (arg === "/") {
        currentPath = "root";
      } else if (arg === "..") {
        const split = currentPath.split(".dirs.");
        currentPath = split.slice(0, split.length - 1).join(".dirs.");
      } else {
        currentPath = `${currentPath}.dirs.${arg}`;
      }
      currentDir = _.get(struct, currentPath, deepClone(defaultDir));
    }
  } else {
    const [sizeType, name] = row.split(" ")

    if (sizeType === "dir") {
      let dir = _.get(currentDir, `dirs.${name}`, deepClone(defaultDir));
      _.set(currentDir, `dirs.${name}`, dir);
      _.set(struct, currentPath, currentDir);
    } else if (sizeType.match(`[0-9]`)) {
      let files = [...currentDir.files];
      files.push({name, size: parseInt(sizeType)});
      _.set(currentDir, 'files', files);
      currentDir.totalSize += parseInt(sizeType, 10);
      _.set(struct, currentPath, currentDir);
    }
  }
}

const calculateDirectorySize = (node, dirs) => {
  if (JSON.stringify(node.dirs) === "{}") {
    return [node.totalSize];
  } else {
    let size = node.totalSize;
    for (let key in node.dirs) {
      let [subSize, subDirs] = calculateDirectorySize(node.dirs[key], []);
      size += subSize;
      dirs = [...dirs, {name: key, size: subSize}];
      if (subDirs) {
        dirs = [...dirs, ...subDirs];
      }
    }

    return [size, dirs];
  }
};

const [totalSize, directories] = calculateDirectorySize(struct.root, []);
console.log("Total size", totalSize);

const sum = directories.filter(x => x.size <= 100000).reduce((acc, dir) => {
  return acc + dir.size;
}, 0);
console.log("Sum of directories", sum);
