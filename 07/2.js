import {deepClone, readInput} from "../helpers.js";
import _ from "lodash/object.js";

const input = readInput(true).split("\n");

const struct = {};
const defaultDir = {
  dirs: {},
  files: [],
  totalSize: 0
};

let currentDir = deepClone(defaultDir);

let currentPath = "root";
for (let row of input) {
  if (row.match(/^\$/)) {
    let [cmd, arg] = row.replace(/^\$ /, '').split(" ");
    if (cmd === "cd") {
      _.set(struct, currentPath, currentDir);

      if (arg === "/") {
        currentPath = "root";
      } else if (arg === "..") {
        const split = currentPath.split(".dirs.");
        currentPath = split.slice(0, split.length - 1).join(".dirs.");
      } else {
        currentPath = `${currentPath}.dirs.${arg}`;
      }
      currentDir = _.get(struct, currentPath, deepClone(defaultDir));
    }
  } else {
    const [sizeType, name] = row.split(" ")

    if (sizeType === "dir") {
      let dir = _.get(currentDir, `dirs.${name}`, deepClone(defaultDir));
      _.set(currentDir, `dirs.${name}`, dir);
      _.set(struct, currentPath, currentDir);
    } else if (sizeType.match(`[0-9]`)) {
      let files = [...currentDir.files];
      files.push({name, size: parseInt(sizeType)});
      _.set(currentDir, 'files', files);
      currentDir.totalSize += parseInt(sizeType, 10);
      _.set(struct, currentPath, currentDir);
    }
  }
}

const calculateDirectorySize = (node, dirs) => {
  if (JSON.stringify(node.dirs) === "{}") {
    return [node.totalSize];
  } else {
    let size = node.totalSize;
    for (let key in node.dirs) {
      let [subSize, subDirs] = calculateDirectorySize(node.dirs[key], []);
      size += subSize;
      dirs = [...dirs, {name: key, size: subSize}];
      if (subDirs) {
        dirs = [...dirs, ...subDirs];
      }
    }

    return [size, dirs];
  }
};

const [totalSize, directories] = calculateDirectorySize(struct.root, []);
const needToFree = 30000000 - (70000000 - totalSize);

const possibleDirs = directories.filter(x => x.size >= needToFree).sort((a, b) => a.size > b.size ? 1 : -1);
console.log("Delete directory", `"${possibleDirs[0].name}"`,"freeing up", possibleDirs[0].size);
