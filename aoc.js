import fs from 'fs';
import fetch from 'isomorphic-fetch';

const session = fs.readFileSync(".session");

const fetchInput = async (day) => {
  return await fetch(`https://adventofcode.com/2022/day/${parseInt(day)}/input`, {
    headers: {
      Cookie: `session=${session}`,
      'User-Agent': 'John\'s AoC init script (root@ranbogmord.com): https://gitlab.com/ranbogmord/aoc-2022/-/blob/master/aoc.js'
    }
  }).then(res => res.text());
};

const createFile = (fileName, content) => {
  if (!fs.existsSync(fileName)) {
    fs.writeFileSync(fileName, content);
  } else {
    console.log("file already exists:", fileName);
  }
};

const createDay = async () => {
  const day = process.argv[2];
  try {
    fs.accessSync(`./${day}`);
  } catch (err) {
    fs.mkdirSync(`./${day}`);
  }

  createFile(`./${day}/index.js`, `import { readInputFile, runPart } from '../helpers.js';

const input = readInputFile();

runPart(1, () => {});
runPart(2, () => {});

`);
  createFile(`./${day}/test.txt`, '');

  if (!fs.existsSync(`./${day}/input.txt`)) {
    const input = await fetchInput(day);
    createFile(`./${day}/input.txt`, input.trim());
  }
};

await createDay();
