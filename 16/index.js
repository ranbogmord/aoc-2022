import {readInputFile, runPart} from '../helpers.js';

const tunnels = {};
const flows = {};
const cache = {};

function cachedDfs (pos, time, opened) {
  const key = [
    pos,
    time,
    ...Object.keys(opened).sort()
  ].join('-');

  if (key in cache) {
    return cache[key];
  }

  cache[key] = dfs(pos, time, opened);
  return cache[key];
}

function dfs (pos, time, opened) {
  const ck = [pos, time, ...Object.keys(opened).sort()].join('-');
  if (cache.hasOwnProperty(ck)) {
    return cache[ck];
  }

  if (time === 0) {
    return 0;
  }

  let score = Math.max(...tunnels[pos].map(tunnel => dfs(tunnel, time - 1, opened)));

  if (flows[pos] > 0 && !(pos in opened)) {
    opened[pos] = true;

    score = Math.max(
      score,
      ((time - 1) * flows[pos]) + dfs(pos, time - 1, opened)
    );
  }

  cache[[pos, time, ...Object.keys(opened).sort()].join('-')] = score;
  return score;
}

readInputFile().split("\n").forEach(x => {
  const match = x.trim().match(/Valve ([A-Z]{2}).*?rate=(\d+).*?valves? (.*)/);

  const valve = match[1];
  flows[valve] = parseInt(match[2]);
  tunnels[valve] = match[3].split(', ');
});

runPart(1, () => {
  const result = dfs('AA', 30, {});
  // console.log(flows, tunnels)
  console.log(result)
});
runPart(2, () => {});

