import {readInput} from "../helpers.js";

const input = readInput(true);

const isUnique = (arr) => {
  return arr.length === arr.reduce((acc, item) => {
    if (!acc.includes(item)) {
      acc.push(item);
    }

    return acc;
  }, []).length;
}

const chunk = [];

let idx = 0;
for (let char of input.split("")) {
  idx += 1;
  chunk.push(char);
  if (chunk.length < 4) {
    continue;
  }

  if (isUnique(chunk)) {
    console.log("chunk is unique", chunk, idx);
    break;
  }
  chunk.shift();
}


