import {deepClone, readInputFile, runPart} from '../helpers.js';
import {Grid, Point} from "../grid.js";

let start = [];
let end = [];
const input = readInputFile().split("\n").map((row, y) => {
  return row.split("").map((col, x) => {
    const point = new Point(x, y, 0);
    if (col === "S") {
      start = point;
      col = "a";
    } else if (col === "E") {
      end = point;
      col = "z";
    }
    point.value = col.charCodeAt(0) - 96;
    return point;
  });
});


runPart(1, () => {
  const grid = new Grid(input);
  const paths = grid.djikstra(start, {
    isTargetPoint: (point) => point.x === end.x && point.y === end.y,
    isWalkable: (neighbour, point) => neighbour.value <= point.value + 1,
  })

  console.log("Part 1:", paths.get(end.toString())?.distance);
});
runPart(2, () => {
  const grid = new Grid(input);
  const paths = grid.djikstra(end, {
    isTargetPoint: (point) => point.value === 1,
    isWalkable: (neighbour, point) => point.value - neighbour.value <= 1
  });

  const lowest = [...paths.values()].filter(p => {
    const point = grid.getByCoords(p.pointStr);
    return point.value === 1 && p.distance !== Infinity;
  }).sort((a, b) => a.distance < b.distance ? -1 : 1).shift();

  console.log("Part 2:", lowest?.distance)
});

