const astar = (grid, sX, sY, eX, eY, isWalkable) => {
  const open = {
    [`${sX}-${sY}`]: {
      key: `${sX}-${sY}`,
      x: sX,
      y: sY,
      gScore: 0,
      value: 0,
      parent: null,
      dir: ''
    }
  };

  let closed = {};

  const getAdjacentNodes = (x, y) => {
    const dirs = [[0, -1, '^'], [-1, 0, '<'], [1, 0, '>'], [0, 1, 'v']];
    const neighbours = dirs.map(dir => [x + dir[0], y + dir[1], dir[2]]);
    const valid = neighbours.filter(n => {
      let [tx, ty] = n;
      return !(tx < 0 || ty < 0 || tx > grid[0].length - 1 || ty > grid.length - 1) && isWalkable(grid, x, y, tx, ty);
    });

    return valid.map(item => {
      return {
        key: `${item[0]}-${item[1]}`,
        x: item[0],
        y: item[1],
        value: grid[item[1]][item[0]],
        dir: item[2]
      };
    });
  };

  const manhattanDistance = (x, y) => {
    return Math.abs(x - eX) + Math.abs(y - eY);
  };

  const calculateNodeScore = (node, prevNode) => {
    let gScore = 1 + prevNode.gScore;
    let hScore = manhattanDistance(node.x, node.y);
    let fScore = gScore + hScore;

    return {
      ...node,
      gScore,
      hScore,
      fScore
    };
  };

  do {
    let current = Object.values(open).sort((a, b) => a.fScore < b.fScore ? -1 : 1).shift();

    if (!current) {
      console.log("no open node found");
      return false;
    }

    delete open[current.key]
    closed[current.key] = current;

    const adjacent = getAdjacentNodes(current.x, current.y).map(node => calculateNodeScore(node, current));
    if (!adjacent.length) {
      console.log("no adjacent nodes found");
      return false;
    }

    for (let adj of adjacent) {
      if (adj.key in closed) {
        continue;
      }

      if (!(adj.key in open)) {
        open[adj.key] = {
          ...adj,
          parent: current.key
        };
        continue;
      }

      const item = {...open[adj.key]};
      if (adj.fScore < item.fScore) {
        open[adj.key].fScore = adj.fScore;
        open[adj.key].parent = current.key;
      }
    }
  } while (Object.values(open).length > 0);

  if (`${end[0]}-${end[1]}` in closed) {
    const path = [];
    let item = closed[`${end[0]}-${end[1]}`];
    while (item?.parent) {
      path.push(item);
      item = closed[item.parent];
    }
    return path;
  } else {
    console.log("no path found");
    return [];
  }
}
