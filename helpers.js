import fs from 'fs';

export function readInput(real = false) {
  return fs.readFileSync(real ? 'input.txt' : 'test.txt').toString();
}

export function readInputFile() {
  return fs.readFileSync(process.argv[2]).toString().trim();
}

export function runPart(part, callback) {
  if (!process.argv[3] || parseInt(process.argv[3], 10) === part) {
    callback();
  }
}

export function chunk (arr, size) {
  const chunked = [];
  for (let i = 0; i < arr.length; i++) {
    const last = chunked[chunked.length - 1];
    if (!last || last.length === size) {
      chunked.push([arr[i]]);
    } else {
      last.push(arr[i]);
    }
  }

  return chunked;
}

export const deepClone = (o) => JSON.parse(JSON.stringify(o));
