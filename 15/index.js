import { readInputFile, runPart } from '../helpers.js';
import {Grid, Point} from "../grid.js";

const input = readInputFile().split("\n").map(x => {
  const [match, sX, sY, bX, bY] = x.match(/x=(-?\d+), y=(-?\d+).*x=(-?\d+), y=(-?\d+)/);

  return {
    sensor: new Point(parseInt(sX), parseInt(sY), "S"),
    beacon: new Point(parseInt(bX), parseInt(bY), "B"),
    dX: Math.abs(sX - bX),
    dY: Math.abs(sY - bY),
    distance: Math.abs(sX - bX) + Math.abs(sY - bY)
  }
});

runPart(1, () => {
  let searchRow = 10;
  if (process.argv[2] === "input.txt") {
    searchRow = 2000000;
  }
  const occupiedPoints = new Set();

  const points = input.filter(p => {
    return (p.sensor.y <= searchRow && p.sensor.y + p.distance >= searchRow) || (p.sensor.y >= searchRow && p.sensor.y - p.distance <= searchRow);
  });

  const beacons = input.filter(x => x.beacon.y === searchRow);

  points.forEach(point => {
    const dY = Math.abs(point.sensor.y - searchRow);
    const layers = point.distance - dY;
    const nodes = 1 + (2 * layers);

    for (let x = point.sensor.x - Math.floor(nodes / 2); x < point.sensor.x + Math.ceil(nodes / 2); x++) {
      let oPoint = new Point(x, searchRow, '#');
      occupiedPoints.add(oPoint.toString());
    }
  });

  beacons.forEach(b => {
    occupiedPoints.delete(b.beacon.toString());
  });

  console.log("Part 1:", occupiedPoints.size);

});
runPart(2, () => {
  let maxXY = 20;
  if (process.argv[2] === "input.txt") {
    maxXY = 4000000;
  }

  let foundPoint = null;
  for (let item of input) {
    const dist = item.distance + 1;
    for (let y = Math.max(item.sensor.y - dist, 0); y < Math.min(item.sensor.y + dist, maxXY); y++) {
      const dY = Math.abs(item.sensor.y - y);

      const layer = dist - dY;
      const nodes = 1 + (layer * 2);

      const left = new Point(item.sensor.x - Math.floor(nodes / 2), y, '#')
      const right = new Point(item.sensor.x + Math.ceil(nodes / 2), y, '#');
      const toCheck = [];

      if (left.x >= 0 && left.x <= maxXY) {
        toCheck.push(left);
      }
      if (right.x !== left.x && right.x >= 0 && right.x <= maxXY) {
        toCheck.push(right);
      }

      if (toCheck.length) {
        for (let item of toCheck) {
          const found = !input.find(x => item.distanceTo(x.sensor) <= x.distance);
          if (found) {
            foundPoint = item;
            break;
          }
        }
      }

      if (foundPoint) {
        break;
      }
    }

    if (foundPoint) {
      break;
    }
  }

  if (!foundPoint) {
    console.log("ERROR: no empty points found");
    return;
  }

  console.log("Part 2:", (parseInt(foundPoint.x) * 4000000) + parseInt(foundPoint.y));
});

