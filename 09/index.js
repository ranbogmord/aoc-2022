import {readInputFile, runPart} from '../helpers.js';

const input = readInputFile().split("\n").map((row) => {
  return row.split(" ");
});

const debug = false;
let state = [{x: 0, y: 0}, {x: 0, y: 0}];

const part1TailPositions = new Set();
const part2TailPositions = new Set();

const printDebug = (state) => {
  if (!debug) {
    return;
  }

  const s = [...state];
  let m = [];
  for (let i = 0; i < 21; i++) {
    let r = [];
    for (let j = 0; j < 26; j++) {
      r.push(".")
    }
    m.push(r);
  }

  s.forEach((item, idx) => {
    if (idx === 0) {
      m[item.y][item.x] = "H";
    } else if (m[item.y][item.x] === ".") {
      m[item.y][item.x] = idx;
    }
  });

  console.log(m.map(x => x.join("")).join("\n") + "\n\n");
};

const followTail = (head, tail) => {
  const touching = [{dx: -1, dy: -1}, {dx: 0, dy: -1}, {dx: 1, dy: -1}, {dx: 1, dy: 0}, {dx: 1, dy: 1}, {dx: 1, dy: 1}, {dx: 0, dy: 1}, {dx: -1, dy: 1}, {dx: -1, dy: 0}, {dx: 0, dy: 0}];
  const moves = [
    // Up-Left diagonal
    {dx: -1, dy: -2, x: -1, y: -1},
    {dx: -2, dy: -1, x: -1, y: -1},
    {dx: -2, dy: -2, x: -1, y: -1},

    // Up-Right diagonal
    {dx: 1, dy: -2, x: 1, y: -1},
    {dx: 2, dy: -1, x: 1, y: -1},
    {dx: 2, dy: -2, x: 1, y: -1},

    // Down-Right diagonal
    {dx: 1, dy: 2, x: 1, y: 1},
    {dx: 2, dy: 1, x: 1, y: 1},
    {dx: 2, dy: 2, x: 1, y: 1},

    // Down-Left diagonal
    {dx: -1, dy: 2, x: -1, y: 1},
    {dx: -2, dy: 1, x: -1, y: 1},
    {dx: -2, dy: 2, x: -1, y: 1},

    // left
    {dx: -2, dy: 0, x: -1, y: 0},
    // Top
    {dx: 0, dy: -2, x: 0, y: -1},
    // Right
    {dx: 2, dy: 0, x: 1, y: 0},
    // Down
    {dx: 0, dy: 2, x: 0, y: 1}
  ];

  let dx = head.x - tail.x;
  let dy = head.y - tail.y;
  if (touching.find(x => x.dx === dx && x.dy === dy)) {
    // touching, abort
    return tail;
  }

  let m = moves.find(x => x.dx === dx && x.dy === dy);
  if (!m) {
    console.error("No move found for:", dx, dy);
    return tail;
  }

  tail.x += m.x;
  tail.y += m.y;
  return tail;
};

const move = (state, result, direction, amount) => {
  const [head, ...tails] = state;

  let tmpState = [];
  for (let i = 0; i < amount; i++) {
    if (direction === 'U') {
      head.y -= 1;
    } else if (direction === "R") {
      head.x += 1;
    } else if (direction === "D") {
      head.y += 1;
    } else if (direction === "L") {
      head.x -= 1;
    }

    let lastHead = head;

    tmpState = [head];
    for (let i = 0; i < tails.length; i++) {
      let t = followTail(lastHead, tails[i]);
      lastHead = t;
      tmpState.push(t);
    }

    result.add(`${tmpState[tmpState.length - 1].x}-${tmpState[tmpState.length - 1].y}`);
    printDebug(tmpState);
  }

  return tmpState;
}

runPart(1, () => {
  part1TailPositions.add(`${state[state.length - 1].x}-${state[state.length - 1].y}`);
  printDebug(state);
  input.forEach(row => {
    state = move(state, part1TailPositions, ...row);
  });
  console.log("Part 1:", part1TailPositions.size);
});

runPart(2, () => {
  let part2State = [];
  for (let i = 0; i < 10; i++) {
    part2State.push({
      x: 0,
      y: 0
    });
  }
  part2TailPositions.add(`${part2State[part2State.length - 1].x}-${part2State[part2State.length - 1].y}`);
  printDebug(part2State);
  input.forEach(row => {
    part2State = move(part2State, part2TailPositions, ...row);
  });
  console.log("Part 2:", part2TailPositions.size);
})
