export class PriorityQueue {
  constructor(initialState = []) {
    this.state = initialState;
  }

  add(item) {
    this.state.push(item);
    this.state.sort((a, b) => a.priority < b.priority ? -1 : 1);
  }

  next() {
    return this.state.shift();
  }

  length() {
    return this.state.length;
  }
}
