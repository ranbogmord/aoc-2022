import { readInputFile, runPart } from '../helpers.js';

const input = readInputFile().replace(/^\n/mg, '|').split("|").map(x => x.trim().split("\n").map(x => JSON.parse(x)));

const parseTree = (left, right) => {
  if (left === undefined) {
    return true;
  }

  if (right === undefined) {
    return false;
  }

  if (typeof left !== typeof right) {
    if (typeof left === "number" && Array.isArray(right)) {
      return parseTree([left], right);
    } else {
      return parseTree(left, [right]);
    }
  }

  if (typeof left === "number" && typeof right === "number") {
    if (left === right) {
      return undefined;
    }

    return left < right;
  }

  for (let i = 0; i < Math.max(left.length, right.length); i++) {
    const result = parseTree(left[i], right[i]);
    if (result === false) {
      return false;
    } else if (result === true) {
      return true;
    }
  }

  return undefined;
};

runPart(1, () => {
  const result = input.reduce((acc, [left, right], idx) => {
    const result = parseTree(left, right);
    if (result) {
      acc += idx + 1;
    }
    return acc;
  }, 0);

  console.log("Part 1:", result);
});
runPart(2, () => {
  const input2 = input.reduce((acc, packet) => {
    packet.forEach(row => acc.push(row));
    return acc;
  }, []);

  input2.push([[2]]);
  input2.push([[6]]);

  const sorted = input2.sort((a, b) => {
    const result = parseTree(a, b);
    if (result) {
      return -1;
    } else {
      return 1
    }
  });

  const strings = sorted.map(x => JSON.stringify(x));

  const result = strings.reduce((acc, row, idx) => {
    if (row === "[[2]]" || row === "[[6]]") {
      return acc * (idx + 1);
    }
    return acc;
  }, 1);

  console.log("Part 2:", result);
});

