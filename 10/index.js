import {chunk, readInputFile, runPart} from '../helpers.js';

const input = readInputFile().split("\n").map(x => x.split(" "));

runPart(1, () => {
  let state = {
    cycles: 0,
    x: 1,
    sum: 0,
  };
  const checkCycles = [20, 60, 100, 140, 180, 220];

  state = input.reduce((s, row) => {
    const [instr, arg] = row;
    s.cycles += 1;
    if (checkCycles.includes(s.cycles)) {
      s.sum += s.x * s.cycles;
    }
    if (instr === "addx") {
      s.cycles += 1;
      if (checkCycles.includes(s.cycles)) {
        s.sum += s.x * s.cycles;
      }
      s.x += parseInt(arg);
    }

    return s;
  }, state);

  console.log("Part 1:", state.sum);
});
runPart(2, () => {
  let state = {
    cycles: 0,
    x: 1,
    row: 0,
    col: 0,
    draw: ''
  };

  state = input.reduce((s, row) => {
    const [instr, arg] = row;

    for (let i = 0; i < (instr === "addx" ? 2 : 1); i++) {
      s.cycles += 1;
      s.row = Math.floor(s.cycles / 40);
      s.col = (s.cycles - 1) % 40;

      if (s.col >= s.x - 1 && s.col <= s.x + 1) {
        // draw
        s.draw += "#";
      } else {
        // no draw
        s.draw += ".";
      }
    }

    if (instr === "addx") {
      s.x += parseInt(arg);
    }

    return s;
  }, state);

  const crt = chunk(state.draw.split(""), 40).map(x => x.join("")).join("\n");

  console.log("Part 2:");
  console.log(crt)
});

